var request = require("request"),
    assert = require('assert'),
    helloWorld = require("../index.js"),
    base_url = "http://localhost:8080/osoblje";

const supertest = require("supertest");
const app = require("../index");


var chai = require('chai');
global.expect = chai.expect;

describe("Server check", function() {
  describe("GET /", function() {
    it("Vraca vrijednost 200 test 7", function(done) {
      request.get(base_url, function(error, response, body) {
        assert.equal(200, response.statusCode);
        done();
      });
    });


    });
  });

  
describe("Server start, test 1", function() {
  describe("GET /", function() {
    it("Vraca vrijednost 200", function(done) {
      request.get(base_url, function(error, response, body) {
        assert.equal(200, response.statusCode);
        done();
      });
    });


    });
  });


  describe('length', function() {
    it('Vraca listu osoblja, test 2', function(done) {
        request.get(base_url, function(err, res, body) {
                expect(res.body).to.have.lengthOf(177);
                done(err);
            });
    });
});


describe("GET /", function() {
    it("Treba imati kod 200, test 3", function(done) {
      supertest(app)
        .get("/")
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
          done();
        });
    });
  });

  describe("Provjeravamo za salje", function() {
    it("Treba biti vrijednost 200, test 4", function(done) {
      supertest(app)
        .get("/sale")
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
          done();
        });
    });
  });

  describe("Provjeravamo za osoblje", function() {
    it("Treba biti vrijednost 200, test 5", function(done) {
      supertest(app)
        .get("/osoblje")
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
          done();
        });
    });
  });

  describe("Da li je dodana rezervacija", function() {
    it("Treba biti vrijednost 200, test 6", function(done) {
      supertest(app)
        .get("/rezervacija")
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
          done();
        });
    });
  });
