const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Rezervacija = sequelize.define("Rezervacija",{
        id:{ type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        createdAt: {
            allowNull: false,
            type: Sequelize.DATE
        },
        updatedAt: {
            allowNull: false,
            type: Sequelize.DATE
        }
    },{
        timeStamps: true 
    })
    return Rezervacija;
};