/*const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19","root","root",{host:"127.0.0.1",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
db.osoblje = sequelize.import(__dirname+'/osoblje.js');
db.rezervacija = sequelize.import(__dirname+'/rezervacija.js');
db.termin = sequelize.import(__dirname+'/termin.js');
db.sala = sequelize.import(__dirname+'/sala.js');

//relacije
db.osoblje.hasMany(db.rezervacija,{foreignKey:'osoba'});
db.termin.hasOne(db.rezervacija, {foreignKey: {name: 'termin', type: Sequelize.INTEGER, unique: 'compositeIndex'}});
//db.termin.hasOne(db.rezervacija,{foreignKey:'termin'});
db.sala.hasMany(db.rezervacija,{foreignKey:'sala'});
db.sala.hasOne(db.osoblje,{foreignKey:'zaduzenaOsoba'});

//belongsto
db.rezervacija.belongsTo(db.osoblje, {foreignKey : 'osoba'});
db.rezervacija.belongsTo(db.termin, {as:'relacijaTermin', foreignKey:{ name:'termin', type: Sequelize.INTEGER , unique: 'compositeIndex'}});
db.rezervacija.belongsTo(db.sala, {foreignKey: 'sala', as: "relacijaSala"});


module.exports=db;*/

const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19","root","",{host:"127.0.0.1",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//Import modela
db.Osoblje = sequelize.import(__dirname+'/osoblje.js');
db.Rezervacija = sequelize.import(__dirname+'/rezervacija.js');
db.Termin = sequelize.import(__dirname+'/termin.js');
db.Sala = sequelize.import(__dirname+'/sala.js');

//Relacije
db.Osoblje.hasMany(db.Rezervacija,{foreignKey: 'osoba'});
db.Termin.hasOne(db.Rezervacija, {foreignKey: {name: 'termin', type: Sequelize.INTEGER, unique: 'compositeIndex'}});
db.Sala.hasMany(db.Rezervacija,{foreignKey: 'sala'});
db.Osoblje.hasOne(db.Sala, {foreignKey: 'zaduzenaOsoba'});

db.Rezervacija.belongsTo(db.Osoblje, {foreignKey : 'osoba'});
db.Rezervacija.belongsTo (db.Termin, {foreignKey:{ name:'termin', type: Sequelize.INTEGER , unique: 'compositeIndex'}});
db.Rezervacija.belongsTo (db.Sala, {foreignKey: 'sala' });
db.Sala.belongsTo(db.Osoblje, {foreignKey: 'zaduzenaOsoba' });

module.exports=db;