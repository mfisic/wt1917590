var express = require('express')
var bodyParser = require('body-parser');
var fs = require('fs');
const path = require('path');

const db = require('./spirala4/db.js')


db.sequelize.sync({force:false}).then(function(){
    console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
    popuni();
});


var app = express();
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('htmlovi'));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/htmlovi/pocetna.html'));
})

app.get('/rezervacija',function (req, res) {

    var niz_prvi=[];
    //console.log("Dosao sam do ovdeeee");

    var niz_vanrednih=[];


    db.Termin.findAll({
        include: [{
            model: db.Rezervacija,
            required: true,
        }]
    })
        .then(termini => {
            termini.map((n) => {
                var skup1 = {};
                //console.log("Dosao sam ovdjeeee vol 2");
                var skup2 = {};

                if (n.dan!==null){
                    skup1.dan = n.dan;
                    skup1.semestar = n.semestar;
                    skup1.pocetak = n.pocetak;
                    skup1.kraj = n.kraj;
                    skup1.naziv = n.Rezervacija.sala;
                    niz_prvi.push(skup2);
                } 
                else{
                    skup2.datum = n.datum;
                    skup2.pocetak = n.pocetak;
                    skup2.kraj = n.kraj;
                    skup2.naziv = n.Rezervacija.sala;
                    niz_vanrednih.push(skup2);
                }

            })

            var noviObjektZaZauzeca = {
                periodicna: niz_prvi,
                vanredna: niz_vanrednih
            }


            res.send(noviObjektZaZauzeca)

        })

});



app.post('/rezervacija', function (req, res) {
    var date = new Date();
    var month = date.getMonth(); // cijeli datum

    let rezervacija = req.body;

    var niz_periodicnih  = [];
    var niz_vanrednih   = [];

    db.Termin.findAll({
        include: [{
            model: db.Rezervacija,
            required: true,
        }]
    })
        .then(termini => {

            termini.map((n) => {
                var skup1 = {};
                // podaci o danu
                var skup2 = {};


                if (n.dan !== null)
                {
                    skup1.dan = n.dan; skup1.semestar = dajSemestar(month);
                    // termin za taj dan
                    skup1.pocetak = n.pocetak;
                    skup1.kraj = n.kraj;
                    // sala za taj dan
                    skup1.naziv = n.Rezervacija.sala;
                    niz_periodicnih.push(skup1);

                } 
                else{
                    skup2.datum = n.datum; skup2.pocetak = n.pocetak;
                    skup2.kraj = n.kraj;
                    // upisuje sve
                    skup2.naziv = n.Rezervacija.sala;
                    niz_vanrednih.push(skup2);
                }

            });
            
            let bodyDatum = rezervacija.datum;
            let bodyPocetak = rezervacija.pocetak;
            let bodyKraj = rezervacija.kraj;
            let bodyNaziv = rezervacija.naziv;


            let bodyPredavac = req.body.predavac;
            let ima_ga = salaZauzeta(niz_vanrednih, bodyNaziv, bodyDatum, bodyPocetak, bodyKraj) || salaZauzeta(niz_periodicnih, bodyNaziv, bodyDatum, bodyPocetak, bodyKraj);

            if (ima_ga) {
                let x = {
                    poruka: "Nije moguće rezervisati salu " + bodyNaziv + " za navedeni datum " + bodyDatum +
                    " i termin od " + bodyPocetak + " do " + bodyKraj + "!"

                };

                res.send(JSON.stringify(x));
            } else {

                let noviRed = {
                    datum: bodyDatum,
                    pocetak: bodyPocetak,
                    kraj: bodyKraj,
                    naziv: bodyNaziv,
                    predavac: bodyPredavac
                }
                niz_vanrednih.push(noviRed);

                let noviObjektZaZauzeca = {
                    periodicna: niz_periodicnih,
                    vanredna: niz_vanrednih
                }
                var varTermin, varRezervacija;

               
               var arr1 = rezervacija.datum.split('.');
                var mjesec = parseInt(arr1[1]);
                varTermin = db.Termin.build({
                    redovni: false,
                    dan: rezervacija.dan,
                    datum: rezervacija.datum,
                    semestar: dajSemestar(mjesec),
                    pocetak: rezervacija.pocetak,
                    kraj: rezervacija.kraj
                });
                varTermin.save()
                    .then(function(){
                        varRezervacija = db.Rezervacija.build({
                            termin: varTermin.id,
                            osoba: rezervacija.predavac,
                            sala: rezervacija.naziv,
                        })
                        varRezervacija.save()
                            .then(function(){
                                //res.end();
                                res.send(noviObjektZaZauzeca);
                            })
                    })
            }

        })
});

app.get('/zauzeca', function (req, res) {

    /*fs.readFile('zauzeca.json', function (err, content) {
        if (err) res.send('Desila se greška');
        //res.send(JSON.parse(content));
        console.log(JSON.parse(content))
    })*/
})

/*SPIRALA 3 JSON */

/* KLIK OVDJEEEEEEEE DA ZAUZMETE SALUUUUUUUUUUUUUUUU */

app.get('/pocetna-slike', function (req, res) {
    const stranica = req.query.stranica || 0;
    const ukupnoStranica = Math.floor(slikeZaPocetnu.length / 3);
    const slike = slikeZaPocetnu.slice(3 * stranica, (3 * stranica) + 3);

    res.send({
        slike,
        stranica,
        ukupnoStranica,
    })
})


app.get('/osobe', function (req, res) {
    var date = new Date();

    var nizZaOsobeISale = [];
    var profesorKancelarija = [];

    db.Osoblje.findAll({
        include: [{
            model: db.Rezervacija,
            include: [
                db.Termin, db.Sala
            ],
        }]
    })
    .then(osoblje => {

        osoblje.forEach(osobe => {

            osobe.Rezervacijas.map(function (rezervacija) {

                var kojaRezervacija = rezervacija.Termin.get().dan;
                var usporedbaDan = date.getDate();
                var sljedeciMjesec = date.getMonth() + 1; // numeracija mjeseca
                var infoSati = date.getHours();

                 if (kojaRezervacija) 
                 {
                     // rezervacija ovdje sigurno nije null
                    if (kojaRezervacija == usporedbaDan) { nizZaOsobeISale.push({ime_osobe: osobe.ime, ime_sale: rezervacija.Sala.get().naziv }); }
                    else { profesorKancelarija.push({ime_osobe: osobe.ime, kancelarija: 'Kancelarija' }); }
                }

                else if (kojaRezervacija === null) 
                {
                    // usli smo samo ako je rezervacija null
                    if (usporedbaDan == rezervacija.Termin.get().datum.split('.')[0] && (sljedeciMjesec == rezervacija.Termin.get().datum.split('.')[1])) 
                    {
                        if (infoSati >= rezervacija.Termin.get().pocetak.substring(0,2) && infoSati <= rezervacija.Termin.get().kraj.substring(0,2)) 
                        {
                            nizZaOsobeISale.push({ime_osobe: osobe.ime, ime_sale: rezervacija.Sala.get().naziv });
                        }
                    } 
                    else { profesorKancelarija.push({ime_osobe: osobe.ime, kancelarija: 'Kancelarija' }); }
                
                } 
              
            })

        })

        res.render('osoblje', {data : nizZaOsobeISale, data1: profesorKancelarija })
    })
});


app.get('/zabiljezi-posjetu', function (req, res) {
    const address = req.connection.remoteAddress;
    const userAgent = req.headers["user-agent"];

    const statistika = JSON.parse(fs.readFileSync("posjete.json"));
    statistika.posjete.push({ address, userAgent });

    fs.writeFileSync("posjete.json", JSON.stringify(statistika));

    const sazetak = dajSazetuStatistiku(statistika);

    res.send(sazetak);
});

function dajSazetuStatistiku(statistika) {
    const UAParser = require("ua-parser-js");

    const vidjeno = [];
    let chrome = 0;
    let firefox = 0;

    for (unos of statistika.posjete) {
        const ua = UAParser(unos.userAgent);

        if (ua.browser.name == "Chrome") chrome++;
        if (ua.browser.name == "Firefox") firefox++;

        if (!vidjeno.includes(unos.address)) {
            vidjeno.push(unos.address);
        }
    }

    let sazetak = {
        posjeta: statistika.posjete.length,
        razlicitihIPAdresa: vidjeno.length,
        chrome,
        firefox,
    };

    return sazetak;
}

const slikeZaPocetnu = [
    "bulbasaur.png",
    "charmander.png",
    "jigglypuff.jpg",
    "lapras.png",
    "mew.jpg",
    "pidgeot.jpg",
    "pikachu.jpg",
    "snorlax.jpg",
    "squirtle.jpg",
    "togepi.jpg",
];

function salaZauzetaPeriodicno(podaci_periodicni, trenutnaSala, mjesec, dan, pocetak, kraj) {
    if (!podaci_periodicni) return false;

    var semestar = dajSemestar(mjesec);

    var unosi = podaci_periodicni.filter(function (periodicni_unos) {
        return trenutnaSala == periodicni_unos.naziv && semestar == periodicni_unos.semestar && dan == periodicni_unos.dan;
    });

    if (unosi.length <= 0) {
        return false;
    }

    return unosi.some(function (unos) {
        return intervalVremena(unos.pocetak, unos.kraj, pocetak, kraj);
    });
}

function salaZauzetaVanredno(podaci_vanredni, trenutnaSala, mjesec, dan, pocetak, kraj) {
    if (!podaci_vanredni) return false;

    var unosi = podaci_vanredni.filter(function (vanredni_unos) {
        var datum = dan + "." + (mjesec + 1) + ".2019.";
        return trenutnaSala == vanredni_unos.naziv && datum == vanredni_unos.datum;
    });

    if (unosi.length <= 0) {
        return false;
    }

    return unosi.some(function (unos) {
        return intervalVremena(unos.pocetak, unos.kraj, pocetak, kraj);
    });
}

function salaZauzeta(trenutnaSala, mjesec, dan, pocetak, kraj) {
    return salaZauzetaPeriodicno(trenutnaSala, mjesec, dan, pocetak, kraj) || salaZauzetaVanredno(trenutnaSala, mjesec, dan, pocetak, kraj)
};

function dajSemestar(mjesec) {
    if ([9, 10, 11, 1].includes(mjesec)) return "zimski";
    if ([2, 3, 4, 5, 6].includes(mjesec)) return "ljetnji";
}

function intervalVremena(salaPoc, salaKr, pocetak, kraj) {
    if (pocetak > salaPoc && pocetak < salaKr) {
        return true;
    }
    else if (pocetak < salaPoc && kraj > salaPoc) {
        return true;
    }
    else if (pocetak == salaPoc || kraj == salaKr) {
        return true;
    }
    return false;
}

//CETVRTA SPIRALA 2.zadatak
// app.get('/rezervacija', function (req, res) {
//     db.rezervacija.findAll({attributes: ['id', 'sala', 'termin', 'osoba']}).then(function(izborRezervacije){
//         res.json(izborRezervacije);
//     });
// });

// OVO NE RADI NAZALOSTTTTTTTT 2. ZADATAK
// let spisakPodataka;



// prvi zadatak
app.get('/osoblje', function (req, res) {
    db.Osoblje.findAll({attributes: ['id', 'ime', 'prezime', 'uloga']}).then(function(izborOsoba){
        res.json(izborOsoba);
    });
});

app.get('/sale', function (req, res) {
    db.Sala.findAll({attributes: ['id', 'naziv']}).then(function(izborSala){
        res.json(izborSala);
    });
});

module.exports=app;
app.listen(8080, () => console.log("Server pokrenut na 8080."));

// Popunjavanje Baze

function popuni() {
    db.Osoblje.create({
        id: 1,
        ime: "Neko",
        prezime: "Nekic",
        uloga: "profesor"
    })
    db.Osoblje.create({
        id: 2,
        ime: "Drugi",
        prezime: "Neko",
        uloga: "asistent"
    })
    db.Osoblje.create({
        id: 3,
        ime: "Test",
        prezime: "Test",
        uloga: "asistent"
    })
    db.Sala.create({
        id: 1,
        naziv: "0-01",
        zaduzenaOsoba: 1
    })
    db.Sala.create({
        id: 2,
        naziv: "0-02",
        zaduzenaOsoba: 2
    })
    db.Termin.create({
        id: 1,
        redovni: "false",
        dan: 6,
        datum: "01.01.2020",
        semestar: "zimski",
        pocetak: "12:00",
        kraj: "13:00"
    })
    db.Termin.create({
        id: 2,
        redovni: "true",
        dan: 0,
        datum: null,
        semestar: "zimski",
        pocetak: "13:00",
        kraj: "14:00"
    })
    db.Rezervacija.create({
        id: 1,
        termin: 1,
        sala: 1,
        osoba: 1
    })
    db.Rezervacija.create({
        id: 2,
        termin: 2,
        sala: 1,
        osoba: 3
    })

}
