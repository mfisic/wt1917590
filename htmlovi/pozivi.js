var Pozivi = (function(){
    var konstruktor = function (){

        return {
            dajZauzeca : function(callbackFunction){
                var ajaxRequest = new XMLHttpRequest();

                ajaxRequest.onreadystatechange = function(){
                    if(ajaxRequest.status === 200 && ajaxRequest.readyState === 4){
                        let res = JSON.parse(ajaxRequest.responseText);

                        podaci_periodicni = res.periodicna;
                        podaci_vanredni = res.vanredna;

                        callbackFunction(podaci_periodicni, podaci_vanredni);
                    }
                }

                ajaxRequest.open('GET', 'http://localhost:8080/zauzeca', true);
                ajaxRequest.setRequestHeader('Content-Type', 'application/json');
                ajaxRequest.send();
            },

            upisiZauzece : function(noviObjekat, callbackFunction) {
                var ajaxRequest = new XMLHttpRequest();

                ajaxRequest.onreadystatechange = function(){
                    if(ajaxRequest.status === 200 && ajaxRequest.readyState === 4){
                        let res = JSON.parse(ajaxRequest.responseText);
                        if(!res.poruka) {
                            podaci_periodicni = res.periodicna;
                            podaci_vanredni = res.vanredna;

                            callbackFunction(podaci_periodicni, podaci_vanredni);
                        }
                        else {
                            alert(JSON.parse(ajaxRequest.responseText).poruka);
                        }
                    }
                }

                ajaxRequest.open('POST', 'http://localhost:8080/rezervacija', true);
                ajaxRequest.setRequestHeader('Content-Type', 'application/json');
                ajaxRequest.send(JSON.stringify(noviObjekat));
            },

            dajSlike : function(stranica, callbackFunction) {
                var ajaxRequest = new XMLHttpRequest();

                ajaxRequest.onreadystatechange = function(){
                    if(ajaxRequest.status === 200 && ajaxRequest.readyState === 4){
                        let res = JSON.parse(ajaxRequest.responseText);
                        callbackFunction(res);
                    }
                }

                ajaxRequest.open('GET', 'http://localhost:8080/pocetna-slike?stranica=' + stranica, true);
                ajaxRequest.setRequestHeader('Content-Type', 'application/json');
                ajaxRequest.send();
            },

            posjeta : function(callbackFunction) {
                var ajaxRequest = new XMLHttpRequest();

                ajaxRequest.onreadystatechange = function(){
                    if(ajaxRequest.status === 200 && ajaxRequest.readyState === 4){
                        let res = JSON.parse(ajaxRequest.responseText);
                        callbackFunction(res);
                    }
                }

                ajaxRequest.open('GET', 'http://localhost:8080/zabiljezi-posjetu', true);
                ajaxRequest.setRequestHeader('Content-Type', 'application/json');
                ajaxRequest.send();
            },
        }
    }

    return konstruktor;
}());

function ucitajOsobe() {

    $.get("/osoblje", function(data, status){
        
        var x = document.getElementsByClassName("predavac")[0];
        for(let i = 0; i < data.length; i++) {

            let id = data[i].id;
            let ime = data[i].ime;
            let prezime = data[i].prezime;
            var option = document.createElement("option");
            option.text = ime + ' ' + prezime;
            option.value = id;
            x.add(option);
        }
    })
}


function ucitajSale() {
    $.get("/sale", function(data, status){

        var x = document.getElementsByClassName("listaSala")[0];
        for(let i = 0; i < data.length; i++) {

            let id = data[i].id;
            let naziv = data[i].naziv;
            var option = document.createElement("option");
            option.value = id;
            option.text = naziv;
            x.add(option);
        }
    })
}


function ucitajRezervacije() {
    $.get("/rezervacija", function(data, status){

        var ajaxRequest = new XMLHttpRequest();
    
        podaci_periodicni = [];
        podaci_vanredni = [];
        podaci_periodicni = data.periodicna;
        podaci_vanredni = data.vanredna;


        Kalendar.updateujTabelu();

        ajaxRequest.open('GET', 'http://localhost:8080/rezervacija', true);
        ajaxRequest.setRequestHeader('Content-Type', 'application/json');
        ajaxRequest.send();

    })
}

