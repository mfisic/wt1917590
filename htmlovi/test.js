let assert = chai.assert; // TESTOVI ZA DRUGI ZADATAK
describe('iscrtajKalendar()', function() {
    it('Provjera redova', function() {
      var kalendar = document.createElement("div");
      Kalendar.iscrtajKalendar(document.getElementById('kalendar'), 0); 
      let tabele = document.getElementsByTagName("table");
      let tabela = tabele[tabele.length-1];
      let red = tabela.getElementsByTagName("tr");
      let kol = red[2].getElementsByTagName("td");
      let dani = 0;
      for(let i=0;i<kol.length;i++) {
        let stil = window.getComputedStyle(kol[i])
        if(stil.display!=='none') dani++;
      }
      assert.equal(dani, 7,"Broj redova treba biti 7");
    }); 
  it('Test za mjesec od 31 dan, u ovom primjeru decembar', function() {
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"),11);
    let dani = document.getElementsByClassName("celijaBroj");
    let brojac = 0;
    for(var i=0; i<dani.length; i++){
          if(dani[i].textContent!='') brojac++;
    }
    assert.equal(brojac, 31,"Trebao bi imati 31 dan");
});
it('Test za mjesec od 30 dana, u ovom primjeru april', function() {
  Kalendar.iscrtajKalendar(document.getElementById("kalendar"),3);
  let dani = document.getElementsByClassName("celijaBroj");
  let brojac = 0;
  for(var i=0; i<dani.length; i++){
        if(dani[i].textContent!='') brojac++;
  }
  assert.equal(brojac, 30,"Trebao bi imati 30 dana");
});
it('Trebao bi imati 28 dana, jer je februar', function() {
  Kalendar.iscrtajKalendar(document.getElementById("kalendar"),1);
  let dani = document.getElementsByClassName("celijaBroj");
  let brojac = 0;
  for(var i=0; i<dani.length; i++){
        if(dani[i].textContent!='') brojac++;
  }
  assert.equal(brojac, 28,"Trebao bi imati 28 dana");
});
it('Trebao bi imati 31 dan (mjesec je januar)', function() {
  Kalendar.iscrtajKalendar(document.getElementById("kalendar"),0);
  let dani = document.getElementsByClassName("celijaBroj");
  let brojac = 0;
  for(var i=0; i<dani.length; i++){
        if(dani[i].textContent!='') brojac++;
  }
  assert.equal(brojac, 31,"Trebao bi imati 31 dan");
});
it('Prvi dan u sedmici je petak', function() {
  // var d = new Date();
  // var n = d.getMonth(); <- mozemo koristiti i ove funkcije, ali za ovaj zadatak znamo da je trenutni mjesec novembar
  Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
  let kal = document.getElementById("tabela");
  let tabela = document.getElementsByTagName('table')[0];
  let dani = 0;
  for(var i=0; i<7; i++){
        if(tabela.rows[2].cells[i].innerHTML!="") break;
        dani++;
  }
  assert.equal(dani, 0, "Prvi dan je petak");
});
})



describe('obojiZauzeca()', function() { // TESTOVI ZA PRVI ZADATAK
  it('Ne boji nista', function() {
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
    let dani = document.getElementsByClassName("slobodna");
    let daniZ = document.getElementsByClassName("zauzeta");
    let sviZeleni = 0;
    for(var i=0; i<dani.length; i++){
          if(daniZ=='') zeleni = 1;
    }
    assert.equal(sviZeleni, 0,"Trebao bi imati 31 dan");
  });
})
