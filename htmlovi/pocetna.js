let PoziviSlike = new Pozivi();

const ucitaneStranice = {};
let trenutnaStranica = 0;
let ukupnoStranica = 0;

function prikaziStranicu(stranica) {
    trenutnaStranica = stranica;
    const parent = document.querySelector(".slike");

    parent.innerHTML = "";

    const slike = dajSlike(stranica);

    if (slike) {
        for (slika of slike) {
            const img = document.createElement("img");
            img.src = slika;
            img.style.width = "250px";
            img.style.height = "250px";
            parent.appendChild(img);
        }
    }

    prikaziKontrole(stranica);
}

function prikaziKontrole(stranica) {
    const parent = document.querySelector(".slikeKontrole");

    parent.innerHTML = "";

    const prethodni = document.createElement("button");
    prethodni.innerText = "Prethodni";
    prethodni.disabled = stranica <= 0;
    prethodni.onclick = proslaStranica;

    const slijedeci = document.createElement("button");
    slijedeci.innerText = "Slijedeći";
    slijedeci.disabled = stranica >= ukupnoStranica;
    slijedeci.onclick = iducaStranica;

    parent.appendChild(prethodni);
    parent.appendChild(slijedeci);
}

function iducaStranica() {
    prikaziStranicu(Math.min(trenutnaStranica + 1, ukupnoStranica));
}

function proslaStranica() {
    prikaziStranicu(Math.max(trenutnaStranica - 1, 0));
}

function dajSlike(stranica) {
    if (ucitaneStranice[stranica]) {
        return ucitaneStranice[stranica];
    }
    ucitajSlike(stranica);
}

function ucitajSlike(stranica) {
    PoziviSlike.dajSlike(stranica, function(res) {
        ucitaneStranice[stranica] = res.slike;
        ukupnoStranica = res.ukupnoStranica;
        prikaziStranicu(stranica);
    });
}

PoziviSlike.posjeta(function(sazetak) {
    const parent = document.querySelector(".posjetioci");
    parent.innerHTML =
        "Posjeta: " + sazetak.posjeta + "<br/>" +
        "Razlicitih adresa: " + sazetak.razlicitihIPAdresa + "<br/>" +
        "Posjeta sa Chromea: " + sazetak.chrome + "<br/>" +
        "Posjeta sa Firefoxa: " + sazetak.firefox + "<br/>";
});

prikaziStranicu(0);
