
var Kalendar = (function () {
  var periodicnaZauzeca;
  var vanrednaZauzeca;

  var sala;
  var periodicna; /*bool*/
  var pocetak;
  var kraj;

  function sljedeciButtonImpl() {

    var mjesec = document.getElementsByClassName('mjesec')[0].innerHTML;
    document.getElementById('kalendar').innerHTML = "";
    console.log(mjesec);
    if (mjesec == "Januar") {
      iscrtajKalendarImpl(document.getElementById('kalendar'), 1);
      document.getElementsByClassName('dugme')[0].disabled = false;
      updateujTabelu();
    }
    else if (mjesec == "Februar") { iscrtajKalendarImpl(document.getElementById('kalendar'), 2); updateujTabelu(); }
    else if (mjesec == "Mart") { iscrtajKalendarImpl(document.getElementById('kalendar'), 3); updateujTabelu(); }
    else if (mjesec == "April") { iscrtajKalendarImpl(document.getElementById('kalendar'), 4); updateujTabelu(); }
    else if (mjesec == "Maj") { iscrtajKalendarImpl(document.getElementById('kalendar'), 5); updateujTabelu(); }
    else if (mjesec == "Juni") { iscrtajKalendarImpl(document.getElementById('kalendar'), 6); updateujTabelu(); }
    else if (mjesec == "Juli") { iscrtajKalendarImpl(document.getElementById('kalendar'), 7); updateujTabelu(); }
    else if (mjesec == "August") { iscrtajKalendarImpl(document.getElementById('kalendar'), 8); updateujTabelu(); }
    else if (mjesec == "Septembar") { iscrtajKalendarImpl(document.getElementById('kalendar'), 9); updateujTabelu(); }
    else if (mjesec == "Oktobar") { iscrtajKalendarImpl(document.getElementById('kalendar'), 10); updateujTabelu(); }
    else if (mjesec == "Novembar") {
      iscrtajKalendarImpl(document.getElementById('kalendar'), 11);
      document.getElementsByClassName('dugme')[1].disabled = true;
      updateujTabelu();
    }
  }

  function prethodniButtonImpl() {
    var mjesec = document.getElementsByClassName('mjesec')[0].innerHTML;

    document.getElementById('kalendar').innerHTML = "";
    if (mjesec == "Februar") {
      iscrtajKalendarImpl(document.getElementById('kalendar'), 0);
      document.getElementsByClassName('dugme')[0].disabled = true;
      updateujTabelu();
    }
    else if (mjesec == "Mart") { iscrtajKalendarImpl(document.getElementById('kalendar'), 1); updateujTabelu(); }
    else if (mjesec == "April") { iscrtajKalendarImpl(document.getElementById('kalendar'), 2); updateujTabelu(); }
    else if (mjesec == "Maj") { iscrtajKalendarImpl(document.getElementById('kalendar'), 3); updateujTabelu(); }
    else if (mjesec == "Juni") { iscrtajKalendarImpl(document.getElementById('kalendar'), 4); updateujTabelu(); }
    else if (mjesec == "Juli") { iscrtajKalendarImpl(document.getElementById('kalendar'), 5); updateujTabelu(); }
    else if (mjesec == "August") { iscrtajKalendarImpl(document.getElementById('kalendar'), 6); updateujTabelu(); }
    else if (mjesec == "Septembar") { iscrtajKalendarImpl(document.getElementById('kalendar'), 7); updateujTabelu(); }
    else if (mjesec == "Oktobar") { iscrtajKalendarImpl(document.getElementById('kalendar'), 8); updateujTabelu(); }
    else if (mjesec == "Novembar") {
      iscrtajKalendarImpl(document.getElementById('kalendar'), 9);
      document.getElementsByClassName('dugme')[1].disabled = false;
      updateujTabelu();

    }
    else if (mjesec == "Decembar") {
      iscrtajKalendarImpl(document.getElementById('kalendar'), 10);
      document.getElementsByClassName('dugme')[1].disabled = false;
      updateujTabelu();
    }
  }

  function dajSemestar(mjesec) {
    if ([9, 10, 11, 0].includes(mjesec)) return "zimski";
    if ([1, 2, 3, 4, 5].includes(mjesec)) return "ljetnji";
  }

  /*function semestarUMjesece(semestar) {
    if (semestar == "zimski") return [9, 10, 11, 0];
    if (semestar == "zimski") return [1, 2, 3, 4, 5];
  }*/

  function salaZauzetaPeriodicno(trenutnaSala, mjesec, dan, pocetak, kraj) {
    if (!podaci_periodicni) return false;

    var semestar = dajSemestar(mjesec);

    var unosi = podaci_periodicni.filter(function(periodicni_unos) {
      return trenutnaSala == periodicni_unos.naziv && semestar == periodicni_unos.semestar && dan == periodicni_unos.dan;
    });

    if (unosi.length <= 0) {
      return false;
    }

    return unosi.some(function(unos) {
      return intervalVremena(unos.pocetak, unos.kraj, pocetak, kraj);
    });
  }

  function salaZauzetaVanredno(trenutnaSala, mjesec, dan, pocetak, kraj) {
    if (!podaci_vanredni) return false;

    var unosi = podaci_vanredni.filter(function(vanredni_unos) {
      var datum = dan + "." + (mjesec + 1) + ".2019.";
      return trenutnaSala == vanredni_unos.naziv && datum == vanredni_unos.datum;
    });

    if (unosi.length <= 0) {
      return false;
    }

    return unosi.some(function(unos) {
      return intervalVremena(unos.pocetak, unos.kraj, pocetak, kraj);
    });
  }

  function salaZauzeta(trenutnaSala, mjesec, dan, pocetak, kraj) {
    return salaZauzetaPeriodicno(trenutnaSala, mjesec, dan, pocetak, kraj) || salaZauzetaVanredno(trenutnaSala, mjesec, dan, pocetak, kraj)
  };

  // OBOJI ZAUZECA
  function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj) {

    var celije = document.querySelectorAll("td:not(.prazni) .celijaTabele");
    var trenutnaSala = document.getElementById("sala").value;

    //console.log("Trenutna sala ", document.getElementById("sala").value )

    for (celija of celije) {
      var dan = celija.querySelector(".celijaBroj").innerText;

      if(salaZauzeta(trenutnaSala, mjesec, dan, pocetak, kraj)) {
        celija.children[1].setAttribute("class", "zauzeta");
      }
    }
  };
  /*function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj) {
    var daniUMjesecu = document.getElementsByClassName("celijaTabele");
    var e = document.getElementById("sala");
    var trenutnaSala = e.options[e.selectedIndex].text;

    for (let i = 0; i < podaci_periodicni.length; i++) {
      // console.log(intervalVremena(podaci_periodicni[i].pocetak, podaci_periodicni[i].kraj, pocetak, kraj))
      if (podaci_periodicni[i].semestar == "zimski" && (mjesec == 0 || mjesec == 9 || mjesec == 10 || mjesec == 11)
        && podaci_periodicni[i].naziv == sala && (intervalVremena(podaci_periodicni[i].pocetak, podaci_periodicni[i].kraj, pocetak, kraj) == false)) {

        if (podaci_periodicni[i].dan == 0) {
          let pomocna = 0;
          for (let j = 0; j < 7; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }
        if (podaci_periodicni[i].dan == 1) {
          let pomocna = 1;
          for (let j = 0; j < 7; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }
        if (podaci_periodicni[i].dan == 2) {
          let pomocna = 2;
          for (let j = 0; j < 6; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }
        if (podaci_periodicni[i].dan == 3) {
          let pomocna = 3;
          for (let j = 0; j < 6; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }
        if (podaci_periodicni[i].dan == 4) {
          let pomocna = 4;
          for (let j = 0; j < 6; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }
        if (podaci_periodicni[i].dan == 5) {

          let pomocna = 5;
          for (let j = 0; j < 6; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }
        if (podaci_periodicni[i].dan == 6) {

          let pomocna = 6;
          for (let j = 0; j < 6; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }

      }
      else if (podaci_periodicni[i].semestar == 'ljetni' && (mjesec == 1 || mjesec == 2 || mjesec == 3 || mjesec == 4 || mjesec == 5)
        && podaci_periodicni[i].naziv == sala && (intervalVremena(podaci_periodicni[i].pocetak, podaci_periodicni[i].kraj, pocetak, kraj) == true) &&
        pocetak != 0 && kraj != 0) {
        if (podaci_periodicni[i].dan == 0) {
          let pomocna = 0;
          for (let j = 0; j < 5; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }
        if (podaci_periodicni[i].dan == 1) {
          let pomocna = 1;
          for (let j = 0; j < 5; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }
        if (podaci_periodicni[i].dan == 2) {
          let pomocna = 2;
          for (let j = 0; j < 5; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }
        if (podaci_periodicni[i].dan == 3) {
          let pomocna = 3;
          for (let j = 0; j < 5; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }
        if (podaci_periodicni[i].dan == 4) {
          let pomocna = 4;
          for (let j = 0; j < 5; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }
        if (podaci_periodicni[i].dan == 5) {
          let pomocna = 5;
          for (let j = 0; j < 5; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }
        if (podaci_periodicni[i].dan == 6) {
          let pomocna = 6;
          for (let j = 0; j < 5; j++) {
            daniUMjesecu[pomocna].children[1].setAttribute("class", "zauzeta");
            pomocna += 7;
          }
        }
      }
    }

    let pomocnaVAR = 0;
    for (let i = 0; i < podaci_vanredni.length; i++) {

      if ((kojiMjesec(podaci_vanredni[i].datum)) == (mjesec + 1) && podaci_vanredni[i].naziv == sala
        && (intervalVremena(podaci_vanredni[i].pocetak, podaci_vanredni[i].kraj, pocetak, kraj) == true) &&
        pocetak != 0 && kraj != 0) {
        let prviUMjesecuDan;
        if (mjesec == 0) prviUMjesecuDan = 1;
        else if (mjesec == 1) prviUMjesecuDan = 4; // feb
        else if (mjesec == 2) prviUMjesecuDan = 4; // mart
        else if (mjesec == 3) prviUMjesecuDan = 0; // april
        else if (mjesec == 4) prviUMjesecuDan = 2; // maj
        else if (mjesec == 5) prviUMjesecuDan = 5; //juni
        else if (mjesec == 6) prviUMjesecuDan = 0;
        else if (mjesec == 7) prviUMjesecuDan = 3;
        else if (mjesec == 8) prviUMjesecuDan = 6;
        else if (mjesec == 9) prviUMjesecuDan = 1;
        else if (mjesec == 10) prviUMjesecuDan = 4;
        else if (mjesec == 11) prviUMjesecuDan = 6;

        for (let k = prviUMjesecuDan; k < 31; k++) {
          pomocnaVAR++;
          if (pomocnaVAR == kojiDan(podaci_vanredni[i].datum)) {
            daniUMjesecu[k].children[1].setAttribute("class", "zauzeta");
            break;
          }
        }
      }
      pomocnaVAR = 0;
    }
  }*/

  function ucitajPodatkeImpl(periodicna, vanredna) {
    this.periodicnaZauzeca = periodicna;
    this.vanrednaZauzeca = vanredna; // napravi funkciju
    obojiZauzecaImpl(document.getElementsByClassName("kalendar"), this.trenutniMjesec, document.getElementsByClassName("listaSala")[0].value, document.getElementById("pocetak").value, document.getElementById("kraj").value);
  }

  function iscrtajKalendarImpl(kalendarRef, mjesec) {
    document.getElementById('kalendar').innerHTML = "";
    var nazivMjeseca = document.createElement("div");
    //nazivMjeseca.setAttribute('id', 'mjesec');
    nazivMjeseca.className = "mjesec";

    if (mjesec == 0) nazivMjeseca.innerHTML = "Januar";
    else if (mjesec == 1) nazivMjeseca.innerHTML = "Februar";
    else if (mjesec == 2) nazivMjeseca.innerHTML = "Mart";
    else if (mjesec == 3) nazivMjeseca.innerHTML = "April";
    else if (mjesec == 4) nazivMjeseca.innerHTML = "Maj";
    else if (mjesec == 5) nazivMjeseca.innerHTML = "Juni";
    else if (mjesec == 6) nazivMjeseca.innerHTML = "Juli";
    else if (mjesec == 7) nazivMjeseca.innerHTML = "August";
    else if (mjesec == 8) nazivMjeseca.innerHTML = "Septembar";
    else if (mjesec == 9) nazivMjeseca.innerHTML = "Oktobar";
    else if (mjesec == 10) nazivMjeseca.innerHTML = "Novembar";
    else if (mjesec == 11) { nazivMjeseca.innerHTML = "Decembar"; document.getElementsByClassName('dugme')[1].disabled = true; }

    kalendarRef.appendChild(nazivMjeseca);

    var i;

    var tab = document.createElement("TABLE");
    tab.setAttribute('class', 'tabela');

    for (i = 0; i < 7; i++) {
      tab.insertRow(i);
    }

    var daniNiz = ["PON", "UTO", "SRI", "ČET", "PET", "SUB", "NED"];
    var daniMjesec = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    var d = new Date(2019, mjesec);
    // Pocinjemo od pon, a ovo ide od nedjelje

    var prviDanMjesec = d.getDay() - 1;
    if (prviDanMjesec == -1) prviDanMjesec = 6;

    for (i = 0; i < tab.rows.length; i++) {
      var trenutniRed = tab.rows[i];

      for (j = 0; j < 7; j++) {
        var celija = trenutniRed.insertCell(j);

        // postavljamo prvi red
        if (i == 0) {
          celija.innerText = daniNiz[j];
          celija.setAttribute('class', 'dani');
        }

        // pravimo strukturu ćelija roditelj -> [dijete, dijete], organizovano kao td: celijaOkvir - [celijaBroj, celijaZauzetost]
        else {

          var celijaOkvir = document.createElement("div");
          var celijaBroj = document.createElement("div");
          var celijaZauzetost = document.createElement("div");

          celijaZauzetost.innerText = "Neki tekst..."; // ako je kontekst diva prazan - div neće se prikazati, ovako dodajemo tekst koji u CSS sklonimo
          // sa color:transparent
          celijaOkvir.setAttribute('class', 'celijaTabele');
          celijaBroj.setAttribute('class', 'celijaBroj');
          celijaBroj.addEventListener('click', celijaBrojClick);
          celijaZauzetost.setAttribute('class', 'slobodna');

          celija.appendChild(celijaOkvir);
          celijaOkvir.appendChild(celijaBroj);
          celijaOkvir.appendChild(celijaZauzetost);
        }
      }
    }
    var br = 1;

    // zapisujem vrijednosti
    for (i = 1; i < tab.rows.length; i++) {
      var trenutniRed = tab.rows[i];

      var j = i == 1 ? prviDanMjesec : 0; // ternarni operator, prvi red

      for (j; j < trenutniRed.cells.length; j++) {

        var trenutnaCelija = trenutniRed.cells[j];
        if (br <= daniMjesec[mjesec]) {
          var celijaDijete = trenutnaCelija.firstChild;
          celijaDijete.firstChild.innerText = br;
          // mijenjam text unutar prvog djeteta - znači onaj sadržaj bijelih divova.
        }
        else {
          trenutnaCelija.setAttribute('class', 'prazni');
        }
        br++;

      }
    }
    // sklanjam još one prazne elemente iz prvog reda tj. mijenjam im klasu, potrebno je obrisati
    // sve one koji su prije prvog dana u mjesecu..

    for (j = 0; j < prviDanMjesec; j++) {
      tab.rows[1].cells[j].setAttribute('class', 'prazni');
      // elementi [1][j] su nevidljivi - display:none
    }

    kalendarRef.appendChild(tab);
  }

  return {
    iscrtajKalendar: iscrtajKalendarImpl,
    sljedeciButton: sljedeciButtonImpl,
    prethodniButton: prethodniButtonImpl,
    obojiZauzeca: obojiZauzecaImpl,
    ucitajPodatke: ucitajPodatkeImpl,
    updateujTabelu: updateujTabelu

  }
}());

//document.getElementById("pocetak").addEventListener("change", updateujTabelu);
//document.getElementById("kraj").addEventListener("change", updateujTabelu);

window.onload = function () {
  // Kalendar.sala = document.getElementById("sala").value;
  // Kalendar.periodicna = document.getElementById("periodicna").checked;
  // Kalendar.pocetak = document.getElementById("pocetak").value;
  // Kalendar.kraj = document.getElementById("kraj").value;
  // Kalendar.periodicna = document.getElementById("periodicna").checked;
  trenutniMjesec = new this.Date().getMonth();
  if(trenutniMjesec == 0) document.getElementsByClassName('dugme')[0].disabled = true;
  Kalendar.iscrtajKalendar(document.getElementById('kalendar'), this.trenutniMjesec);
  Kalendar.ucitajPodatke(dajPeriodicna(), []);
  ucitajOsobe();
  ucitajSale();
  ucitajRezervacije();
  Kalendar.obojiZauzeca(document.getElementById('kalendar'), this.trenutniMjesec, Kalendar.pocetak, Kalendar.kraj);
}
var trenutniMjesec;

function updateujTabelu() {
  // Kalendar.sala = document.getElementById("sala").value;
  // Kalendar.periodicna = document.getElementById("periodicna").checked;
  // Kalendar.pocetak = document.getElementById("pocetak").value;
  // Kalendar.kraj = document.getElementById("kraj").value;
  let m = document.getElementsByClassName('mjesec')[0].innerHTML;
  if (m == "Januar") m = 0;
  else if (m == "Februar") m = 1;
  else if (m == "Mart") m = 2;
  else if (m == "April") m = 3;
  else if (m == "Maj") m = 4;
  else if (m == "Juni") m = 5;
  else if (m == "Juli") m = 6;
  else if (m == "August") m = 7;
  else if (m == "Septembar") m = 8;
  else if (m == "Oktobar") m = 9;
  else if (m == "Novembar") m = 10;
  else if (m == "Decembar") m = 11;
  Kalendar.iscrtajKalendar(document.getElementById('kalendar'), m);
  //Kalendar.iscrtajKalendar(document.getElementById('kalendar'), trenutniMj);
  // Kalendar.ucitajPodatke(dajPeriodicna(), []);
  // Kalendar.ucitajPodatke(dajVanredna(), []);
  //Kalendar.obojiZauzeca(document.getElementById('kalendar'), this.trenutniMjesec, Kalendar.sala, Kalendar.pocetak, Kalendar.kraj);
  // Kalendar.ucitajPodatke(podaci_periodicni, podaci_vanredni);
  Kalendar.obojiZauzeca(document.getElementsByClassName("kalendar"), m, document.getElementsByClassName("listaSala")[0].value, document.getElementById("pocetak").value, document.getElementById("kraj").value);

}

function dajVrijemeSati(nekoVrijeme) {
  var prva = nekoVrijeme[0];
  var druga = nekoVrijeme[1];

  return (parseInt(prva + druga));
}

function dajVrijemeMinute(nekoVrijeme) {
  var prva = nekoVrijeme[3];
  var druga = nekoVrijeme[4];

  return parseInt(prva + druga);
}

function vrijemeDomen(pocetakZauzeca, krajZauzeca, pocetakInput, krajInput) {
  return uporediVremena(pocetakInput, pocetakZauzeca) && uporediVremena(krajZauzeca, krajInput);
}

function uporediVremena(a, b) {
  return new Date('1/1/1999 ' + a + ':00') <= new Date('1/1/1999 ' + b + ':00');
}

function intervalVremena(salaPoc, salaKr, pocetak, kraj) {
  if (pocetak > salaPoc && pocetak < salaKr) {
    return true;
  }
  else if (pocetak < salaPoc && kraj > salaPoc) {
    return true;
  }
  else if (pocetak == salaPoc || kraj == salaKr) {
    return true;
  }
  return false;
}


